﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class KanjiDisplayEntity : MonoBehaviour
{
    Rigidbody2D entityRigidbody;
    private float impulse;
    private float TTL = 4f;
    public TextMeshPro textmeshPro;

    // Start is called before the first frame update
    void Start()
    {
        //Physics Based 
        impulse = Random.Range(10.0f, 14.0f);
        entityRigidbody = GetComponent<Rigidbody2D>();
        entityRigidbody.AddForce(transform.up * impulse, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        //Deletes the game object after 4 seconds
        TTL -= Time.deltaTime;
        if (TTL <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void setText(string text)
    {
        textmeshPro.SetText(text);
    }
}
