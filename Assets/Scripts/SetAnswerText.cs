﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAnswerText : MonoBehaviour
{
    public Text answerReadings;

    public void setAnswer(string answer)
    {
        answerReadings.text = answer;
    }
}
