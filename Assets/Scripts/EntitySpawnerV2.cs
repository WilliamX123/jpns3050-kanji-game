﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntitySpawnerV2 : MonoBehaviour
{
    public KanjiSpriteEntity kanjiPrefab;
    public KanjiSpriteEntity kanjiAnswerPrefab;
    public JPLT5_Dictionary dictionary;
    public SetAnswerText kanaPrefab;
    public Rect spawnRect;

    private float timer = 4f;
    private string diffculty; 
    private int entityCount;
    private int answerVal;
    private int randomVal;
    private int lastAnswer;

    void OnDrawGizmos()
    {
        // Draw the spawning rectangle
        // For visualisation only, not required in final product or game
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    // Start is called before the first frame update
    void Start()
    {
        diffculty = SceneManager.GetActiveScene().name;
        dictionary = Object.FindObjectOfType<JPLT5_Dictionary>();
        if(diffculty == "JPLT_5_EASY") // Get scene name instead,
        {
            entityCount = 3;
        }
        if (diffculty == "JPLT_5_MEDIUM")
        {
            entityCount = 4;
        }
        if (diffculty == "JPLT_5_HARD")
        {
            entityCount = 5;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            spawnAnswererSet();
            for (int i = 0; i < entityCount - 1; i++)
            {
                randomVal = Random.Range(
                    0, JPLT5_Dictionary.kanjiDictionary.getInstance().returnSize());
                spawnRandomEntity(i);
            }
            
            timer = 4f;
        }
    }

    void spawnAnswererSet()
    {
        while (answerVal == lastAnswer)
        {
           answerVal = Random.Range(
               0, JPLT5_Dictionary.kanjiDictionary.getInstance().returnSize());
        }
        // instantiate answer set
        KanjiSpriteEntity answerKanji = Instantiate(kanjiAnswerPrefab);
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;
        answerKanji.transform.position = new Vector2(x, y);
        answerKanji.gameObject.name = "Answer Entity";

        //sets entity text
        answerKanji.GetComponent<KanjiSpriteEntity>().setText(
            JPLT5_Dictionary.kanjiDictionary.getInstance().randomKanji(answerVal));

        //Sets the prefab to true for answer value
        answerKanji.GetComponent<KanjiSpriteEntity>().answer = true;

        //Rembers the last answer set so it does not repeat
        lastAnswer = answerVal;

        kanaPrefab.GetComponent<SetAnswerText>().setAnswer(
            JPLT5_Dictionary.kanjiDictionary.getInstance().randomReading(answerVal));


    }

    void spawnRandomEntity(int i)
    {
        while (randomVal == answerVal)
        {
            randomVal = Random.Range(
                0, JPLT5_Dictionary.kanjiDictionary.getInstance().returnSize());
        }
        //instantiate random set
        KanjiSpriteEntity Kanji = Instantiate(kanjiPrefab);
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;
        Kanji.transform.position = new Vector2(x, y);

        Kanji.gameObject.name = "Kanji Entity";
        Kanji.GetComponent<KanjiSpriteEntity>().setText(
            JPLT5_Dictionary.kanjiDictionary.getInstance().randomKanji(randomVal));
    }
}
