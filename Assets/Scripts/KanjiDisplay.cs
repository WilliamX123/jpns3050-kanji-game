﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KanjiDisplay : MonoBehaviour
{
    public KanjiDisplayEntity kanjiPrefab;
    public JPLT5_Dictionary dictionary;
    public Rect spawnRect;

    private float timer = 4f;
    private int entityCount = 6 ;
    private int randomVal;


    void OnDrawGizmos()
    {
        // Draw the spawning rectangle
        // Delete in final product
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        dictionary = Object.FindObjectOfType<JPLT5_Dictionary>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            for (int i = 0; i < entityCount - 1; i++)
            {
                randomVal = Random.Range(
                    0, JPLT5_Dictionary.kanjiDictionary.getInstance().returnSize());
                spawnRandomEntity(i);
            }
            timer = 4f;
        }
    }

    void spawnRandomEntity(int i)
    {
        //instantiate random set
        KanjiDisplayEntity Kanji = Instantiate(kanjiPrefab);
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;
        Kanji.transform.position = new Vector2(x, y);

        Kanji.gameObject.name = "Kanji Entity";
        Kanji.GetComponent<KanjiDisplayEntity>().setText(
            JPLT5_Dictionary.kanjiDictionary.getInstance().randomKanji(randomVal));
    }
}
