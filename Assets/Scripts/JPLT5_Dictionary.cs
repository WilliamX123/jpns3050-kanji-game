﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JPLT5_Dictionary : MonoBehaviour
{
    public class kanjiDictionary
    {
        private static kanjiDictionary dict;
        public List<Kanji> kanjiArray = new List<Kanji>();

        public static kanjiDictionary getInstance()
        {
            if(dict == null)
            {
                return new kanjiDictionary();
            }
            return dict;
        }

        public kanjiDictionary()
        {
            //kanjiArray.Add(new Kanji("", ""));
            kanjiArray.Add(new Kanji("日", "ひ、ニチ、ジツ"));
            kanjiArray.Add(new Kanji("一", "ひと(つ)、イチ"));
            kanjiArray.Add(new Kanji("国", "くに、コク"));
            kanjiArray.Add(new Kanji("人", "ひと、ジン、ニン"));
            kanjiArray.Add(new Kanji("年", "とし、ネン"));
            kanjiArray.Add(new Kanji("大", "おお（きい）、ダイ、タイ"));
            kanjiArray.Add(new Kanji("十", "とお、と、シュウ"));
            kanjiArray.Add(new Kanji("二", "ふた（つ）、ニ"));
            kanjiArray.Add(new Kanji("本", "もと、ホン"));
            kanjiArray.Add(new Kanji("中", "なか、うち、あた（る）、チュウ"));
            kanjiArray.Add(new Kanji("長", "なが（い）、おさ"));
            kanjiArray.Add(new Kanji("出", "で（る）、だ（す）、い（でる）、シュツ、スイ"));
            kanjiArray.Add(new Kanji("三", "み（つ）、サン"));
            kanjiArray.Add(new Kanji("時", "とき、ジ"));
            kanjiArray.Add(new Kanji("行", "い（く）、ゆ（く）、おこな（う）、コウ、ギョウ、アン"));
            kanjiArray.Add(new Kanji("見", "み（る）、み（せる）、ケン"));
            kanjiArray.Add(new Kanji("月", "つき、ゲツ、ガツ"));
            kanjiArray.Add(new Kanji("分", "わ（ける）、ブン、フン、ブ"));
            kanjiArray.Add(new Kanji("後", "のち、うしろ、あと、ゴ、コウ"));
            kanjiArray.Add(new Kanji("前", "まえ、ゼン"));
            kanjiArray.Add(new Kanji("生", "う（む）、なま、セイ、ショウ"));
            kanjiArray.Add(new Kanji("五", "いつ（つ）、ゴ"));
            kanjiArray.Add(new Kanji("間", "あいだ、ま、あい、カン、ケン"));
            kanjiArray.Add(new Kanji("上", "うえ、あ（げる）、のぼ（る）、ショウ、ショウ"));
            kanjiArray.Add(new Kanji("東", "ひがし、トウ"));
            kanjiArray.Add(new Kanji("四", "よ（つ）、よん、シ"));
            kanjiArray.Add(new Kanji("今", "いま、コン、キン"));
            kanjiArray.Add(new Kanji("金", "かね、キン、コン、ゴン"));
            kanjiArray.Add(new Kanji("九", "ここの（つ）、キュウ、ク"));
            kanjiArray.Add(new Kanji("入", "い（る）、はい（る）、ニュウ"));
            kanjiArray.Add(new Kanji("学", "まな（ぶ）、ガク"));
            kanjiArray.Add(new Kanji("高", "たか（い）、コウ"));
            kanjiArray.Add(new Kanji("円", "まる（い）、エン"));
            kanjiArray.Add(new Kanji("子", "こ、ね、シ、ス、ツ"));
            kanjiArray.Add(new Kanji("外", "そと、ほか、はず（す）、ガイ、ゲ"));
            kanjiArray.Add(new Kanji("八", "や（つ）、よう、ハチ"));
            kanjiArray.Add(new Kanji("六", "む（つ）、むい、ロク"));
            kanjiArray.Add(new Kanji("下", "した、しも、もと、さ（げる）、くだ（る）、お（ろす）、カ、ゲ"));
            kanjiArray.Add(new Kanji("来", "く（る）、きたる、き、こ、ライ、タイ"));
            kanjiArray.Add(new Kanji("気", "いき、キ、ケ"));
            kanjiArray.Add(new Kanji("小", "ちい(さい)、こ-、お-、さ-、ショウ"));
            kanjiArray.Add(new Kanji("七", "なな(つ)、なの、シチ"));
            kanjiArray.Add(new Kanji("山", "やま、サン、セン"));
            kanjiArray.Add(new Kanji("話", "はな(す)、はなし、ワ"));
            kanjiArray.Add(new Kanji("女", "おんな、め、ジョ"));
            kanjiArray.Add(new Kanji("北", "きた、ホク"));
            kanjiArray.Add(new Kanji("午", "うま、ゴ"));
            kanjiArray.Add(new Kanji("百", "もも、ヒャク、ビャク"));
            kanjiArray.Add(new Kanji("書", "か(く)、ショ"));
            kanjiArray.Add(new Kanji("先", "さき、 ま(ず)、セン"));
            kanjiArray.Add(new Kanji("名", "な、メイ、ミョウ"));
            kanjiArray.Add(new Kanji("川", "かわ、セン"));
            kanjiArray.Add(new Kanji("千", "ち、セン"));
            kanjiArray.Add(new Kanji("水", "みず、スイ"));
            kanjiArray.Add(new Kanji("半", "なか(ば)、ハン"));
            kanjiArray.Add(new Kanji("男", "おとこ、お、ダン、ナン"));
            kanjiArray.Add(new Kanji("西", "にし、セイ、サイ"));
            kanjiArray.Add(new Kanji("電", "デン"));
            kanjiArray.Add(new Kanji("校", "コウ"));
            kanjiArray.Add(new Kanji("語", "かた(る)、ゴ"));
            kanjiArray.Add(new Kanji("土", "つち、ド、ト"));
            kanjiArray.Add(new Kanji("木", "き、こ-、ボク、モク"));
            kanjiArray.Add(new Kanji("聞", "き(く)、ブン、モン"));
            kanjiArray.Add(new Kanji("食", "く(う)、た(べる)、は(む)、ショク、ジキ"));
            kanjiArray.Add(new Kanji("車", "くるま、シャ"));
            kanjiArray.Add(new Kanji("何", "なに、なん、カ"));
            kanjiArray.Add(new Kanji("南", "みなみ、ナン、ナ"));
            kanjiArray.Add(new Kanji("万", "マン、バン"));
            kanjiArray.Add(new Kanji("毎", "ごと(に)、マイ"));
            kanjiArray.Add(new Kanji("白", "しろ(い)、ハク、ビャク"));
            kanjiArray.Add(new Kanji("天", "あまつ、テン"));
            kanjiArray.Add(new Kanji("母", "はは、かあ、ボ"));
            kanjiArray.Add(new Kanji("火", "ひ、-び、ほ-、カ"));
            kanjiArray.Add(new Kanji("右", "みぎ、ウ、ユウ"));
            kanjiArray.Add(new Kanji("読", "よ(む)、ドク、トク、トウ"));
            kanjiArray.Add(new Kanji("友", "とも、ユウ"));
            kanjiArray.Add(new Kanji("左", "ひだり、サ、シャ"));
            kanjiArray.Add(new Kanji("休", "やす(む)、キュウ"));
            kanjiArray.Add(new Kanji("父", "ちち、とう、フ"));
            kanjiArray.Add(new Kanji("雨", "あめ、 あま、ウ"));
        }

        public string randomKanji(int i)
        {   
            return kanjiArray[i].character;
        }

        public string randomReading(int i)
        {
            return kanjiArray[i].reading;
        }

        public int returnSize()
        {
            return kanjiArray.Count;
        }
    }

    public class Kanji
    {
        public string character;
        public string reading;

        public Kanji(string character, string reading)
        {
            this.character = character;
            this.reading = reading;
        }
    }
}
