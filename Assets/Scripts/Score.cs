﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public int score;
    public Text scoreText;
    private float timeMultpiplyer = 1;
    private int previouseScore = 0;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        scoreText.text = "点数： " + score.ToString();
    }

    public void updateScore(int points)
    {
        score += points;
        scoreText.text = "点数： " + score.ToString();

        
        //As time goes on, increase time scale to speed up process
        if (score % 50 == 0 && previouseScore < score)
        {
            timeMultpiplyer = timeMultpiplyer + 0.1f;
            Time.timeScale = 1 * timeMultpiplyer;
        }
        previouseScore = score;
    }
}
