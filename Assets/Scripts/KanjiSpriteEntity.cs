﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class KanjiSpriteEntity : MonoBehaviour
{
    Rigidbody2D entityRigidbody;
    private float impulse;
    private float TTL = 4f;
    public bool answer;
    private Score pointsText;
    private LivesScript lives;
    public TextMeshPro textmeshPro;

    public AudioClip correct;
    public AudioClip incorrect;
    private new AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        //Physics Based 
        impulse = Random.Range(10.0f, 14.0f);

        audio = GetComponent<AudioSource>();

        entityRigidbody = GetComponent<Rigidbody2D>();
        entityRigidbody.AddForce(transform.up * impulse, ForceMode2D.Impulse);
        pointsText = Object.FindObjectOfType<Score>();
        lives = Object.FindObjectOfType<LivesScript>();
    }

    // Update is called once per frame
    void Update()
    {

        if(Time.timeScale == 0)
        {
            return;
        }

        //Deletes the game object after 4 seconds
        TTL -= Time.deltaTime;
        if (TTL <= 0)
        {
            if (answer == true)
            {
                //Remove Life
                pointsText.updateScore(-5);
                lives.updateLives(-1);
            }
            Destroy(gameObject);
        }
    }

    void OnMouseDown()
    {
        if (Time.timeScale == 0)
        {
            return;
        }

        if (answer == true)
        {
            //Call scoreboard and add points
            pointsText.updateScore(5);
            audio.PlayOneShot(correct);
            Destroy(gameObject, correct.length);
        }
        else
        {
            //Call scoreboard and remove life
            pointsText.updateScore(-5);
            lives.updateLives(-1);
            audio.PlayOneShot(incorrect);
            Destroy(gameObject, incorrect.length);
        }
    }

    public void setText(string text)
    {
        textmeshPro.SetText(text);
    }
}
