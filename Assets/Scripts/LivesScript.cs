﻿using UnityEngine;
using UnityEngine.UI;

public class LivesScript : MonoBehaviour
{
    public int lives = 3;
    public Text lifeText;
    public GameObject gameOverMenue;

    // Start is called before the first frame update
    void Start()
    {
        
        lifeText.text = "ライフ： " + lives.ToString();
    }

    void Update()
    {
        if(lives < 0)
        {
            //Pause game
            gameOverMenue.SetActive(true);
            Time.timeScale = 0;
            //Create canvas and ingame shell menue
            //Activate restart or main menue UI
        }
    }

    public void updateLives(int life)
    {
        lives += life;
        lifeText.text = "ライフ： " + lives.ToString();
    }
}
