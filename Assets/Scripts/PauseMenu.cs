﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pausePanel;
    private bool paused;


    // Start is called before the first frame update
    void Start()
    {
        SetPaused(false);
    }

    // Update is called once per frame
    void Update()
    {
        // pause if the player presses escape
        if (!paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }
    }

    private void SetPaused(bool p)
    {
        // make the shell panel (in)active when (un)paused
        paused = p;
        Time.timeScale = paused ? 0 : 1;
        pausePanel.SetActive(paused);

    }

    public void OnPressedContinue()
    {
        SetPaused(false);
    }

    public void OnPressedReturn()
    {
        SetPaused(false);
        SceneManager.LoadScene("Menu Scene");
    }

    public void OnPressedRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
