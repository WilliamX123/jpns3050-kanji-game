﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShellMenu : MonoBehaviour
{
    public GameObject difficultyPanel;
    public GameObject levelSelectPanel;
    private int level;

    public void OnPressedJPLT5()
    {
        difficultyPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        level = 5;
    }

    public void OnPressedJPLT4()
    {
        difficultyPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        level = 4;
    }

    public void OnPressedJPLT3()
    {
        difficultyPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        level = 3;
    }

    public void OnPressedJPLT2()
    {
        difficultyPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        level = 2;
    }

    public void OnPressedJPLT1()
    {
        difficultyPanel.SetActive(true);
        levelSelectPanel.SetActive(false);
        level = 1;
    }

    public void OnPressedQuit()
    {
        Application.Quit();
    }


    public void OnPressedEasy()
    {
        Debug.Log("Easy pressed");
        if (level == 5) {
            Debug.Log("JPLT 5 EASY");
            SceneManager.LoadScene("JPLT_5_EASY");
        } else if (level == 4)
        {
            SceneManager.LoadScene("JPLT_4_EASY");
        }else if(level == 3)
        {
            SceneManager.LoadScene("JPLT_3_EASY");
        }else if(level == 2)
        {
            SceneManager.LoadScene("JPLT_2_EASY");
        }
        else{
            SceneManager.LoadScene("JPLT_1_EASY");
        }
        
    }

    public void OnPressedMedium()
    {
        if (level == 5)
        {
            SceneManager.LoadScene("JPLT_5_MEDIUM");
        }
        else if (level == 4)
        {
            SceneManager.LoadScene("JPLT_4_MEDIUM");
        }
        else if (level == 3)
        {
            SceneManager.LoadScene("JPLT_3_MEDIUM");
        }
        else if (level == 2)
        {
            SceneManager.LoadScene("JPLT_2_MEDIUM");
        }
        else
        {
            SceneManager.LoadScene("JPLT_1_MEDIUM");
        }

    }

    public void OnPressedHard()
    {
        if (level == 5)
        {
            SceneManager.LoadScene("JPLT_5_HARD");
        }
        else if (level == 4)
        {
            SceneManager.LoadScene("JPLT_4_HARD");
        }
        else if (level == 3)
        {
            SceneManager.LoadScene("JPLT_3_HARD");
        }
        else if (level == 2)
        {
            SceneManager.LoadScene("JPLT_2_HARD");
        }
        else
        {
            SceneManager.LoadScene("JPLT_1_HARD");
        }

    }
}
